package upload

import (
	"bytes"
	"crypto/tls"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"mime/multipart"
	"net/http"
	"net/textproto"
	"os"
	"strings"
	"time"

	"bitbucket.org/jaimeparedes/bc_sii_pkg/config"
)

var client *http.Client

func NewFileRequest(code string, params map[string]string, paramName, path string, token string, env string) ([]byte, error) {
	// HTTP client
	if client == nil {
		client = &http.Client{
			Timeout: 5 * time.Second,
			Transport: &http.Transport{
				TLSClientConfig: &tls.Config{
					InsecureSkipVerify: true,
				},
			},
		}
	}
	// URL
	url := strings.Replace(config.UploadTicketURLCert, "apicert.", "pangal.", 1)
	if env == "production" {
		url = strings.Replace(config.UploadTicketURLProd, "api.", "rahue.", 1)
	}
	if code != "39" && code != "41" {
		url = config.UploadURLCert
		if env == "production" {
			url = config.UploadURLProd
		}
	}
	if code == "AEC" {
		url = config.UploadAecURLCert
		if env == "production" {
			url = config.UploadAecURLProd
		}
	}
	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	// Add params
	for key, val := range params {
		_ = writer.WriteField(key, val)
	}
	// Read file
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	// Add file as param
	xmlFile, _ := CreateXMLFormFile(writer, "envio.xml")
	io.Copy(xmlFile, file)
	err = writer.Close()
	if err != nil {
		return nil, err
	}
	fmt.Println(body)
	req, err := http.NewRequest("POST", url, body)
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Content-Type", writer.FormDataContentType())
	req.Header.Set("Cookie", "TOKEN="+token)
	req.Header.Set("User-Agent", "Mozilla/4.0 ( compatible; PROG 1.0; Windows NT)")
	req.Header.Set("Connection", "close")
	res, err := client.Do(req)
	if err != nil {
		log.Println("Error dispatching the request. ", err.Error())
		return nil, err
	}
	defer res.Body.Close()
	// log.Println("-> Retrieving and parsing the response")
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Println("Error reading response. ", err.Error())
		return nil, err
	}
	return data, nil
}

func CreateXMLFormFile(w *multipart.Writer, filename string) (io.Writer, error) {
	h := make(textproto.MIMEHeader)
	h.Set("Content-Disposition", fmt.Sprintf(`form-data; name="%s"; filename="%s"`, "file", filename))
	h.Set("Content-Type", "text/xml")
	return w.CreatePart(h)
}
