package upload

import (
	"fmt"
	"os"
	"testing"
)

func TestNewFileRequest(t *testing.T) {
	path, _ := os.Getwd()
	path += "/test_data/envio.xml"
	fmt.Println(path)
	extraParams := map[string]string{
		"rutCompany": "76326028",
		"dvCompany":  "3",
		"rutSender":  "17240862",
		"dvSender":   "1",
	}
	token := "QM5ZZ4K4VQVXZ"
	result, err := NewFileRequest("39", extraParams, "archivo", path, token, "certification")
	if err != nil {
		t.Fatalf("Test error. %s", err.Error())
	}
	fmt.Println(string(result))
}
