package claim

import (
	"bytes"
	"crypto/tls"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/jaimeparedes/bc_sii_pkg/config"
)

var client *http.Client

func Dte(issuerCode string, code string, number int, token string, actionCode string, env string) ([]byte, error) {
	// HTTP client
	if client == nil {
		client = &http.Client{
			Timeout: 5 * time.Second,
			Transport: &http.Transport{
				TLSClientConfig: &tls.Config{
					InsecureSkipVerify: true,
				},
			},
		}
	}
	url := config.ClaimDteCert
	if env == "production" {
		url = config.ClaimDteProd
	}
	issuerSlice := strings.Split(issuerCode, "-")
	claimDteTemplate := config.ClaimDteTemplate
	claimDteTemplate = strings.Replace(claimDteTemplate, "@rutEmisor", issuerSlice[0], 1)
	claimDteTemplate = strings.Replace(claimDteTemplate, "@dvEmisor", issuerSlice[len(issuerSlice)-1], 1)
	claimDteTemplate = strings.Replace(claimDteTemplate, "@tipoDoc", code, 1)
	claimDteTemplate = strings.Replace(claimDteTemplate, "@folio", strconv.Itoa(number), 1)
	claimDteTemplate = strings.Replace(claimDteTemplate, "@accionDoc", actionCode, 1)
	claimDteTemplate = strings.Replace(claimDteTemplate, "@token", token, 1)
	body := []byte(strings.TrimSpace(claimDteTemplate))
	fmt.Println(string(body))

	// Prepare the request
	req, err := http.NewRequest("POST", url, bytes.NewReader(body))
	if err != nil {
		log.Println("Error creating request object. ", err.Error())
		return nil, err
	}

	// Set the Content-type header, as well as the other required headers
	req.Header.Set("Accept", "application/xml")
	req.Header.Set("Content-Type", "text/xml;charset=UTF-8")
	req.Header.Set("Cookie", "TOKEN="+token)
	req.Header.Set("SOAPAction", "")
	req.Header.Set("Connection", "close")

	// Dispatch the request
	res, err := client.Do(req)
	if err != nil {
		log.Println("Error dispatching the request. ", err.Error())
		return nil, err
	}
	defer res.Body.Close()
	// log.Println("-> Retrieving and parsing the response")
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Println("Error reading response. ", err.Error())
		return nil, err
	}
	return data, nil
}
