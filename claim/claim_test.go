package claim

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"testing"

	"bitbucket.org/jaimeparedes/bc_sii_pkg/login"
)

type Company struct {
	Code int `json:"code"`
	Data struct {
		Rut         string `json:"rut"`
		Certificate struct {
			Base64   string `json:"certificate"`
			Password string `json:"password"`
		} `json:"certificate"`
	} `json:"data"`
}

func TestClaim(t *testing.T) {
	response, err := http.Get("http://3.22.31.221:8080/companies/26?expand=certificate")
	if err != nil {
		t.Fatalf("Test error. %s", err.Error())
	}
	companyData, _ := ioutil.ReadAll(response.Body)
	var company Company
	json.Unmarshal([]byte(string(companyData)), &company)
	token, err := login.AuthWebService(company.Data.Certificate.Base64, company.Data.Certificate.Password, "certification")
	if err != nil {
		t.Fatalf("Test error. %s", err.Error())
	}
	fmt.Println("TOKEN ~> " + token)
	result, err := Dte("15286665-8", "33", 123, token, "ACD", "certification")
	if err != nil {
		t.Fatalf("Test error. %s", err.Error())
	}
	fmt.Println(string(result))
}
