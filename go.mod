module bitbucket.org/jaimeparedes/bc_sii_pkg

go 1.14

require (
	github.com/antchfx/xmlquery v1.2.3
	github.com/antchfx/xpath v1.1.11 // indirect
	github.com/golang/groupcache v0.0.0-20200121045136-8c9f03a8e57e // indirect
	golang.org/x/net v0.0.0-20201224014010-6772e930b67b // indirect
	golang.org/x/text v0.3.5 // indirect
)
