package config

// Constants
const (
	// Certification
	SeedURLCert         = "https://maullin.sii.cl/DTEWS/CrSeed.jws?WSDL"
	SeedTicketURLCert   = "https://apicert.sii.cl/recursos/v1/boleta.electronica.semilla"
	TokenURLCert        = "https://maullin.sii.cl/DTEWS/GetTokenFromSeed.jws?WSDL"
	TokenTicketURLCert  = "https://apicert.sii.cl/recursos/v1/boleta.electronica.token"
	UploadTicketURLCert = "https://apicert.sii.cl/recursos/v1/boleta.electronica.envio"
	UploadURLCert       = "https://maullin.sii.cl/cgi_dte/UPL/DTEUpload"
	UploadAecURLCert    = "https://maullin.sii.cl/cgi_rtc/RTC/RTCAnotEnvio.cgi"
	TicketURLCert       = "https://apicert.sii.cl/recursos/v1/boleta.electronica"
	QueryUploadURLCert  = "https://maullin.sii.cl/DTEWS/QueryEstUp.jws?WSDL"
	QueryDteURLCert     = "https://maullin.sii.cl/DTEWS/QueryEstDte.jws?WSDL"
	ClaimDteCert        = "https://ws2.sii.cl/WSREGISTRORECLAMODTECERT/registroreclamodteservice"

	// Production
	SeedURLProd         = "https://palena.sii.cl/DTEWS/CrSeed.jws?WSDL"
	SeedTicketURLProd   = "https://api.sii.cl/recursos/v1/boleta.electronica.semilla"
	TokenURLProd        = "https://palena.sii.cl/DTEWS/GetTokenFromSeed.jws?WSDL"
	TokenTicketURLProd  = "https://api.sii.cl/recursos/v1/boleta.electronica.token"
	UploadTicketURLProd = "https://api.sii.cl/recursos/v1/boleta.electronica.envio"
	UploadURLProd       = "https://palena.sii.cl/cgi_dte/UPL/DTEUpload"
	UploadAecURLProd    = "https://palena.sii.cl/cgi_rtc/RTC/RTCAnotEnvio.cgi"
	TicketURLProd       = "https://api.sii.cl/recursos/v1/boleta.electronica"
	QueryUploadURLProd  = "https://palena.sii.cl/DTEWS/QueryEstUp.jws?WSDL"
	QueryDteURLProd     = "https://palena.sii.cl/DTEWS/QueryEstDte.jws?WSDL"
	ClaimDteProd        = "https://ws1.sii.cl/WSREGISTRORECLAMODTE/registroreclamodteservice"

	SeedTemplate = `
	<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:def="http://DefaultNamespace">
		<soapenv:Header/>
		<soapenv:Body>
			<def:getSeed soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/>
		</soapenv:Body>
	</soapenv:Envelope>`
	TokenTemplate = `
	<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:def="http://DefaultNamespace">
		<soapenv:Header/>
		<soapenv:Body>
			<def:getToken soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
				<pszXml xsi:type="xsd:string"><![CDATA[@pszXML]]></pszXml>
			</def:getToken>
		</soapenv:Body>
	</soapenv:Envelope>`
	PszXML = `
	<getToken>
		<item>
			<Semilla>@seed</Semilla>
		</item>
		<Signature xmlns="http://www.w3.org/2000/09/xmldsig#">
			<SignedInfo>
				<CanonicalizationMethod Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315"/>
				<SignatureMethod Algorithm="http://www.w3.org/2000/09/xmldsig#rsa-sha1"/>
				<Reference URI="">
					<Transforms>
						<Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature"/>
					</Transforms>
					<DigestMethod Algorithm="http://www.w3.org/2000/09/xmldsig#sha1"/>
					<DigestValue></DigestValue>
				</Reference>
			</SignedInfo>
			<SignatureValue/>
			<KeyInfo>
				<KeyValue/>
				<X509Data><X509Certificate/></X509Data>
			</KeyInfo>
		</Signature>
	</getToken>`
	QueryUploadTemplate = `
	<env:Envelope xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:impl="http://DefaultNamespace" xmlns:env="http://schemas.xmlsoap.org/soap/envelope/">
		<env:Body>
			<getEstUp>
				<rut>@rut</rut>
				<dv>@dv</dv>
				<trackId>@trackId</trackId>
				<token>@token</token>
			</getEstUp>
		</env:Body>
	</env:Envelope>
	`
	QueryDteTemplate = `
	<env:Envelope xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:impl="http://DefaultNamespace" xmlns:env="http://schemas.xmlsoap.org/soap/envelope/">
		<env:Body>
			<getEstDte>
				<rutConsultante>@rutConsultante</rutConsultante>
				<dvConsultante>@dvConsultante</dvConsultante>
				<rutCompania>@rutCompania</rutCompania>
				<dvCompania>@dvCompania</dvCompania>
				<rutReceptor>@rutReceptor</rutReceptor>
				<dvReceptor>@dvReceptor</dvReceptor>
				<tipoDte>@tipoDte</tipoDte>
				<folioDte>@folioDte</folioDte>
				<fechaEmisionDte>@fechaEmisionDte</fechaEmisionDte>
				<montoDte>@montoDte</montoDte>
				<token>@token</token>
			</getEstDte>
		</env:Body>
	</env:Envelope>
	`
	ClaimDteTemplate = `
	<env:Envelope xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ns0="http://ws.registroreclamodte.diii.sdi.sii.cl" xmlns:env="http://schemas.xmlsoap.org/soap/envelope/">
		<env:Body>
			<ns0:ingresarAceptacionReclamoDoc>
				<rutEmisor>@rutEmisor</rutEmisor>
				<dvEmisor>@dvEmisor</dvEmisor>
				<tipoDoc>@tipoDoc</tipoDoc>
				<folio>@folio</folio>
				<accionDoc>@accionDoc</accionDoc>
			</ns0:ingresarAceptacionReclamoDoc>
		</env:Body>
	</env:Envelope>
	`
)
