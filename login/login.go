package login

import (
	"encoding/xml"
	"errors"
	"fmt"
	"log"
	"strings"

	"bitbucket.org/jaimeparedes/bc_sii_pkg/config"
	"bitbucket.org/jaimeparedes/bc_sii_pkg/dsig"
	"bitbucket.org/jaimeparedes/bc_sii_pkg/soap"
	"bitbucket.org/jaimeparedes/bc_sii_pkg/util"

	"github.com/antchfx/xmlquery"
)

type seed struct {
	XMLName xml.Name
	Body    struct {
		XMLName         xml.Name
		GetSeedResponse struct {
			XMLName       xml.Name
			GetSeedReturn string `xml:"getSeedReturn"`
		} `xml:"getSeedResponse"`
	}
}
type token struct {
	XMLName xml.Name
	Body    struct {
		XMLName          xml.Name
		GetTokenResponse struct {
			XMLName        xml.Name
			GetTokenReturn string `xml:"getTokenReturn"`
		} `xml:"getTokenResponse"`
	}
}

// AuthWebService implements SII authentication using soap webservices
func AuthWebService(certBase64 string, password string, env string) (string, error) {
	var response []byte
	var err error
	url := config.SeedURLCert
	if env == "production" {
		url = config.SeedURLProd
	}
	body := []byte(strings.TrimSpace(config.SeedTemplate))
	retries := 3
	for retries > 0 {
		response, err = soap.Request(url, body)
		if strings.Contains(string(response), "503 ") {
			err = errors.New("503 Service Unavailable")
		}
		if err != nil {
			retries--
			if retries == 0 {
				return "", err
			}
		} else {
			break
		}
	}
	// Parse response to xml struct
	var seed seed
	err = xml.Unmarshal([]byte(string(response)), &seed)
	if err != nil {
		log.Println("Error unmarshalling xml. ", err.Error())
		return "", err
	}
	responseNode, err := xmlquery.Parse(strings.NewReader(seed.Body.GetSeedResponse.GetSeedReturn))
	if err != nil {
		log.Println("Error reading seed. ", err.Error())
		return "", err
	}
	seedNode := xmlquery.FindOne(responseNode, "//SEMILLA")
	// log.Println("SEMILLA:", seedNode.InnerText())
	pszXML := strings.Replace(config.PszXML, "@seed", seedNode.InnerText(), 1)
	// Sign pszXML and return the generated file like a byte array
	pszSigned, err := dsig.Sign(certBase64, password, pszXML, "", "")
	if err != nil {
		return "", err
	}
	pszXML = strings.Replace(config.TokenTemplate, "@pszXML", string(pszSigned), 1)
	url = config.TokenURLCert
	if env == "production" {
		url = config.TokenURLProd
	}
	body = []byte(strings.TrimSpace(pszXML))
	retries = 3
	for retries > 0 {
		response, err = soap.Request(url, body)
		if strings.Contains(string(response), "503 ") {
			err = errors.New("503 Service Unavailable")
		}
		if err != nil {
			retries--
			if retries == 0 {
				return "", err
			}
		} else {
			break
		}
	}
	// Parse response to xml struct
	var token token
	err = xml.Unmarshal([]byte(string(response)), &token)
	if err != nil {
		log.Println("Error unmarshalling xml. ", err.Error())
		return "", err
	}
	responseNode, err = xmlquery.Parse(strings.NewReader(token.Body.GetTokenResponse.GetTokenReturn))
	if err != nil {
		log.Println("Error reading token. ", err.Error())
		return "", err
	}
	result := xmlquery.FindOne(responseNode, "//TOKEN")
	if result == nil {
		result = xmlquery.FindOne(responseNode, "//GLOSA")
		if result != nil {
			err = errors.New(result.InnerText())
		} else {
			err = errors.New("Can't get token from SII response")
		}
		return "", err
	}
	// log.Println("TOKEN:", result.InnerText())
	return result.InnerText(), nil
}

// GetTokenTicket get token from SII for a ticket
func AuthWebServiceTicket(certBase64 string, password string, env string) (string, error) {
	var response *util.Response
	var err error
	url := config.SeedTicketURLCert
	if env == "production" {
		url = config.SeedTicketURLProd
	}
	headers := map[string]string{
		"Accept":     "application/xml",
		"Connection": "close",
	}
	body := []byte{}
	request := util.Request{
		Method:  "GET",
		BaseURL: url,
		Headers: headers,
		Body:    body,
	}
	retries := 3
	for retries > 0 {
		response, err = util.APICall(request)
		if err != nil {
			fmt.Println(err)
		}
		if strings.Contains(string(response.Body), "503 ") {
			err = errors.New("503 Service Unavailable")
		}
		if err != nil {
			retries--
			if retries == 0 {
				return "", err
			}
		} else {
			break
		}
	}
	responseNode, err := xmlquery.Parse(strings.NewReader(string(response.Body)))
	if err != nil {
		log.Println("Error reading seed. ", err.Error())
		return "", err
	}
	seedNode := xmlquery.FindOne(responseNode, "//SEMILLA")
	//fmt.Println("SEMILLA ~> " + seedNode.InnerText())
	pszXML := strings.Replace(config.PszXML, "@seed", seedNode.InnerText(), 1)
	// Sign pszXML and return the generated file like a byte array
	pszSigned, err := dsig.Sign(certBase64, password, pszXML, "", "")
	if err != nil {
		return "", err
	}
	url = config.TokenTicketURLCert
	if env == "production" {
		url = config.TokenTicketURLProd
	}
	headers = map[string]string{
		"Content-Type": "application/xml",
		"Accept":       "application/xml",
		"Connection":   "close",
	}
	body = []byte(string(pszSigned))
	request = util.Request{
		Method:  "POST",
		BaseURL: url,
		Headers: headers,
		Body:    body,
	}
	retries = 3
	for retries > 0 {
		response, err = util.APICall(request)
		if err != nil {
			fmt.Println(err)
		}
		if strings.Contains(string(response.Body), "503 ") {
			err = errors.New("503 Service Unavailable")
		}
		if err != nil {
			retries--
			if retries == 0 {
				return "", err
			}
		} else {
			break
		}
	}
	responseNode, err = xmlquery.Parse(strings.NewReader(string(response.Body)))
	if err != nil {
		log.Println("Error reading token. ", err.Error())
		return "", err
	}
	result := xmlquery.FindOne(responseNode, "//TOKEN")
	if result == nil {
		result = xmlquery.FindOne(responseNode, "//GLOSA")
		if result != nil {
			err = errors.New(result.InnerText())
		} else {
			err = errors.New("Can't get token from SII response")
		}
		return "", err
	}
	//fmt.Println("TOKEN ~> " + result.InnerText())
	return result.InnerText(), nil
}
