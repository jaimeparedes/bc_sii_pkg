package status

import (
	"errors"
	"fmt"
	"strconv"
	"strings"

	"bitbucket.org/jaimeparedes/bc_sii_pkg/config"
	"bitbucket.org/jaimeparedes/bc_sii_pkg/soap"
	"bitbucket.org/jaimeparedes/bc_sii_pkg/util"
)

func Upload(companyCode string, trackID string, token string, env string) (string, error) {
	var response []byte
	var err error
	url := config.QueryUploadURLCert
	if env == "production" {
		url = config.QueryUploadURLProd
	}
	companySlice := strings.Split(companyCode, "-")
	queryUploadTemplate := config.QueryUploadTemplate
	queryUploadTemplate = strings.Replace(queryUploadTemplate, "@rut", companySlice[0], 1)
	queryUploadTemplate = strings.Replace(queryUploadTemplate, "@dv", companySlice[len(companySlice)-1], 1)
	queryUploadTemplate = strings.Replace(queryUploadTemplate, "@trackId", trackID, 1)
	queryUploadTemplate = strings.Replace(queryUploadTemplate, "@token", token, 1)
	body := []byte(strings.TrimSpace(queryUploadTemplate))
	fmt.Println(string(body))
	retries := 3
	for retries > 0 {
		response, err = soap.Request(url, body)
		if strings.Contains(string(response), "503") {
			err = errors.New("503 Service Unavailable")
		}
		if err != nil {
			retries--
			if retries == 0 {
				return "", err
			}
		} else {
			break
		}
	}
	return string(response), nil
}

func Dte(companyCode string, receiverCode string, senderCode string, code string, number int, amount int, date string, token string, env string) (string, error) {
	var response []byte
	var err error
	url := config.QueryDteURLCert
	if env == "production" {
		url = config.QueryDteURLProd
	}
	companySlice := strings.Split(companyCode, "-")
	receiverSlice := strings.Split(receiverCode, "-")
	senderSlice := strings.Split(senderCode, "-")
	queryDteTemplate := config.QueryDteTemplate
	queryDteTemplate = strings.Replace(queryDteTemplate, "@rutConsultante", senderSlice[0], 1)
	queryDteTemplate = strings.Replace(queryDteTemplate, "@dvConsultante", senderSlice[len(senderSlice)-1], 1)
	queryDteTemplate = strings.Replace(queryDteTemplate, "@rutCompania", companySlice[0], 1)
	queryDteTemplate = strings.Replace(queryDteTemplate, "@dvCompania", companySlice[len(companySlice)-1], 1)
	queryDteTemplate = strings.Replace(queryDteTemplate, "@rutReceptor", receiverSlice[0], 1)
	queryDteTemplate = strings.Replace(queryDteTemplate, "@dvReceptor", receiverSlice[len(receiverSlice)-1], 1)
	queryDteTemplate = strings.Replace(queryDteTemplate, "@tipoDte", code, 1)
	queryDteTemplate = strings.Replace(queryDteTemplate, "@folioDte", strconv.Itoa(number), 1)
	queryDteTemplate = strings.Replace(queryDteTemplate, "@fechaEmisionDte", date, 1)
	queryDteTemplate = strings.Replace(queryDteTemplate, "@montoDte", strconv.Itoa(amount), 1)
	queryDteTemplate = strings.Replace(queryDteTemplate, "@token", token, 1)
	body := []byte(strings.TrimSpace(queryDteTemplate))
	fmt.Println(string(body))
	retries := 3
	for retries > 0 {
		response, err = soap.Request(url, body)
		if strings.Contains(string(response), "503") {
			err = errors.New("503 Service Unavailable")
		}
		if err != nil {
			retries--
			if retries == 0 {
				return "", err
			}
		} else {
			break
		}
	}
	return string(response), nil
}

func UploadTicket(companyCode string, trackID string, token string, env string) ([]byte, error) {
	var response *util.Response
	var err error
	url := config.UploadTicketURLCert
	if env == "production" {
		url = config.UploadTicketURLProd
	}
	url = fmt.Sprintf("%s/%s-%s", url, companyCode, trackID)
	fmt.Println(url)
	headers := map[string]string{
		"Accept":     "application/json",
		"Cookie":     "TOKEN=" + token,
		"Connection": "close",
	}
	body := []byte{}
	request := util.Request{
		Method:  "GET",
		BaseURL: url,
		Headers: headers,
		Body:    body,
	}
	retries := 3
	for retries > 0 {
		response, err = util.APICall(request)
		if err != nil {
			fmt.Println(err)
		}
		if strings.Contains(string(response.Body), "503") {
			err = errors.New("503 Service Unavailable")
		}
		if err != nil {
			retries--
			if retries == 0 {
				return nil, err
			}
		} else {
			break
		}
	}
	return response.Body, nil
}

func Ticket(companyCode string, receiverCode string, code string, number int, amount int, date string, token string, env string) ([]byte, error) {
	var response *util.Response
	var err error
	receiverSlice := strings.Split(receiverCode, "-")
	url := config.TicketURLCert
	if env == "production" {
		url = config.TicketURLProd
	}
	url = fmt.Sprintf("%s/%s-%s-%s/estado?rut_receptor=%s&dv_receptor=%s&monto=%s&fechaEmision=%s", url, companyCode, code, strconv.Itoa(number), receiverSlice[0], receiverSlice[len(receiverSlice)-1], strconv.Itoa(amount), date)
	fmt.Println(url)
	headers := map[string]string{
		"Accept":     "application/json",
		"Cookie":     "TOKEN=" + token,
		"Connection": "close",
	}
	body := []byte{}
	request := util.Request{
		Method:  "GET",
		BaseURL: url,
		Headers: headers,
		Body:    body,
	}
	retries := 3
	for retries > 0 {
		response, err = util.APICall(request)
		if err != nil {
			fmt.Println(err)
		}
		if strings.Contains(string(response.Body), "503") {
			err = errors.New("503 Service Unavailable")
		}
		if err != nil {
			retries--
			if retries == 0 {
				return nil, err
			}
		} else {
			break
		}
	}
	return response.Body, nil
}
