package status

import (
	"fmt"
	"testing"
)

type UploadResponse struct {
	RutEmisor      string `json:"rut_emisor"`
	RutEnvia       string `json:"rut_envia"`
	TrackID        string `json:"trackid"`
	FechaRecepcion string `json:"fecha_recepcion"`
	Estado         string `json:"estado"`
	Estadistica    []struct {
		Tipo       string `json:"tipo"`
		Informados int    `json:"informados"`
		Aceptados  int    `json:"aceptados"`
		Rechazados int    `json:"rechazados"`
		Reparos    int    `json:"reparos"`
	} `json:"estadistica,omitempty"`
	Detalle []struct {
		Tipo        string `json:"tipo"`
		Folio       int    `json:"folio"`
		Estado      string `json:"estado"`
		Descripcion string `json:"descripcion"`
		Error       []struct {
			Descripcion string `json:"descripcion"`
		} `json:"error,omitempty"`
	} `json:"detalle_rep_rech,omitempty"`
}

// func TestUploadTicket(t *testing.T) {
// 	token := "QM5ZZ4K4VQVXZ"
// 	result, err := UploadTicket("76326028-3", "123707", token, "certification")
// 	if err != nil {
// 		t.Fatalf("Test error. %s", err.Error())
// 	}
// 	fmt.Println(string(result))
// 	var response UploadResponse
// 	json.Unmarshal([]byte(string(result)), &response)
// 	fmt.Println(response.Estado)
// }

// func TestTicket(t *testing.T) {
// 	token := "QM5ZZ4K4VQVXZ"
// 	result, err := Ticket("76326028-3", "18427496-5", "39", 2927, 1190, "25-08-2020", token, "certification")
// 	if err != nil {
// 		t.Fatalf("Test error. %s", err.Error())
// 	}
// 	fmt.Println(string(result))
// }

func TestUpload(t *testing.T) {
	token := "6O4JKHC00HWL9"
	result, err := Upload("77054815-2", "5318318392", token, "production")
	if err != nil {
		t.Fatalf("Test error. %s", err.Error())
	}
	fmt.Println(string(result))
}

func TestDte(t *testing.T) {
	token := "JXCPYL50IPXTQ"
	result, err := Dte("76326028-3", "76749644-3", "17240862-1", "33", 2431, 1190, "13042021", token, "certification")
	if err != nil {
		t.Fatalf("Test error. %s", err.Error())
	}
	fmt.Println(string(result))
}
