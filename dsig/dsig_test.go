package dsig

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"testing"
)

type Company struct {
	Code int `json:"code"`
	Data struct {
		Rut         string `json:"rut"`
		Certificate struct {
			Base64   string `json:"certificate"`
			Password string `json:"password"`
		} `json:"certificate"`
	} `json:"data"`
}

func TestSign(t *testing.T) {
	response, err := http.Get("http://157.230.182.16:8181/companies/6?expand=certificate")
	if err != nil {
		t.Fatalf("Test error. %s", err.Error())
	}
	companyData, _ := ioutil.ReadAll(response.Body)
	var company Company
	json.Unmarshal([]byte(string(companyData)), &company)
	currentPath, err := os.Getwd()
	if err != nil {
		log.Println(err)
	}
	xmlPath := fmt.Sprintf("%s/test_data/dte.xml", currentPath)
	xmlFile, err := ioutil.ReadFile(xmlPath)
	if err != nil {
		log.Println(err)
	}
	xPath := "/DTE"
	dtdFile := fmt.Sprintf("%s/test_data/sign_dte.dtd", currentPath)
	result, err := Sign(company.Data.Certificate.Base64, company.Data.Certificate.Password, string(xmlFile), xPath, dtdFile)
	if err != nil {
		t.Fatalf("Test error. %s", err.Error())
	}
	fmt.Println(string(result))
}
