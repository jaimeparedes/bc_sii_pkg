package dsig

import (
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strings"
)

// Sign xml files using xmlsec1 library
func Sign(certBase64 string, password string, xmlData string, xPath string, dtdFile string) ([]byte, error) {
	// Decode certificate base64
	pfxData, err := base64.StdEncoding.DecodeString(certBase64)
	if err != nil {
		log.Println("Error decoding certificate. ", err.Error())
		return nil, err
	}
	tmpFolder, err := ioutil.TempDir("", "tmp")
	if err != nil {
		log.Println("TempDir error", err.Error())
	}
	pfxFile := filepath.Join(tmpFolder, "cert.pfx")
	keyFile := filepath.Join(tmpFolder, fmt.Sprintf("%s.key", password))
	crtFile := filepath.Join(tmpFolder, fmt.Sprintf("%s.crt", password))
	p12File := filepath.Join(tmpFolder, fmt.Sprintf("%s.p12", password))
	xmlFile := filepath.Join(tmpFolder, "file.xml")
	signedFile := filepath.Join(tmpFolder, "file_signed.xml")
	// Save certificate to disk
	err = ioutil.WriteFile(pfxFile, pfxData, 0755)
	if err != nil {
		log.Println("cert.pfx", err.Error())
	}
	executable := "bash"
	argument := "-c"
	if runtime.GOOS == "windows" {
		executable = "cmd.exe"
		argument = "/C"
	}
	// Generate .key file
	cmd := fmt.Sprintf("openssl pkcs12 -in '%s' -nocerts -out '%s' -passin 'pass:%s' -passout 'pass:%s'", pfxFile, keyFile, password, password)
	if runtime.GOOS == "windows" {
		cmd = strings.ReplaceAll(cmd, "\\", "\\\\")
	}
	_, err = exec.Command(executable, argument, cmd).Output()
	if err != nil {
		log.Println("Error generating .key file in openssl command. ", err.Error())
		return nil, err
	}
	// Generate .crt file
	cmd = fmt.Sprintf("openssl pkcs12 -in '%s' -clcerts -nokeys -out '%s' -passin 'pass:%s'", pfxFile, crtFile, password)
	if runtime.GOOS == "windows" {
		cmd = strings.ReplaceAll(cmd, "\\", "\\\\")
	}
	_, err = exec.Command(executable, argument, cmd).Output()
	if err != nil {
		log.Println("Error generating .crt file in openssl command. ", err.Error())
		return nil, err
	}
	// Generate .p12 file
	cmd = fmt.Sprintf("openssl pkcs12 -export -out '%s' -inkey '%s' -in '%s' -passin 'pass:%s' -passout 'pass:%s'", p12File, keyFile, crtFile, password, password)
	if runtime.GOOS == "windows" {
		cmd = strings.ReplaceAll(cmd, "\\", "\\\\")
	}
	_, err = exec.Command(executable, argument, cmd).Output()
	if err != nil {
		log.Println("Error generating .p12 file in openssl command. ", err.Error())
		return nil, err
	}
	// Save xml to disk
	err = ioutil.WriteFile(xmlFile, []byte(xmlData), 0755)
	if err != nil {
		log.Println("file.xml", err.Error())
	}
	// Generate signed file
	if xPath != "" {
		cmd = fmt.Sprintf("xmlsec1 --sign --dtd-file %s --output %s --pkcs12 '%s' --pwd '%s' --node-xpath %s %s", dtdFile, signedFile, p12File, password, xPath, xmlFile)
	} else {
		cmd = fmt.Sprintf("xmlsec1 --sign --output %s --pkcs12 '%s' --pwd '%s' %s", signedFile, p12File, password, xmlFile)
	}
	if runtime.GOOS == "windows" {
		cmd = strings.ReplaceAll(cmd, "\\", "\\\\")
	}
	_, err = exec.Command(executable, argument, cmd).Output()
	if err != nil {
		log.Println("Error in xmlsec1 command. ", err.Error())
		return nil, err
	}
	fileSigned, err := ioutil.ReadFile(signedFile)
	if err != nil {
		log.Println("Error reading file_signed.xml. ", err.Error())
		return nil, err
	}
	// Remove temporary files
	os.Remove(pfxFile)
	os.Remove(keyFile)
	os.Remove(crtFile)
	os.Remove(p12File)
	os.Remove(xmlFile)
	os.Remove(signedFile)
	err = os.RemoveAll(tmpFolder)
	if err != nil {
		log.Println("Error removing tmpFolder. ", err.Error())
	} else {
		log.Println("tmpFolder removed")
	}
	return fileSigned, nil
}
