bc-sii-pkg
------------------
bc-sii-pkg is a Go client library for accessing SII (Servicio de Impuestos Internos) automated processes.
Currently, bc-sii-pkg **requires Go version 1.14 or greater.**

## Usage

Importing the package from an external file.

```go
package main
import "bitbucket.org/jaimeparedes/bc_sii_pkg/login"

func main(){
    // PFX file base 64 encoded
    certBase64 := os.Args[1]
    // PFX password
    certPass := os.Args[2]

    token, err := login.TestAuthWebServiceTicket(certBase64, certPass, "production")

    if err != nil || token == "" {
        fmt.Println("Error finding token: ", err)
    } else {
        fmt.Println(token)
    }
}
```

Running the corresponding test.

**login/webservice_test.go**

```sh
go test -run TestAuthWebService
```